function initMap(){

    var mymap = L.map('visor').setView([4.519624, -74.122631], 12);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYWN1bmEiLCJhIjoiY2tjcXJ0MmxoMTdycjJ5czVxY3E3eHUzMyJ9.CU58HkUF9DpAS4tGCVycBQ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
    }).addTo(mymap);

    //panaderia

    var circle = L.circle([4.522102, -74.121200], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 5
    }).addTo(mymap);

    var customIcon = new L.Icon({
        iconUrl: 'https://fornipastisseriasardenya.com/wp-content/uploads/2017/11/icono-ubicacion.png',
        iconSize: [50, 50],
        iconAnchor: [25, 50]
    });
    var marker = L.marker([4.522102, -74.121200], {icon: customIcon}).addTo(mymap);
    marker.bindPopup("Panaderia Will Pan").openPopup();
      
    

    //salitre
    var marker = L.marker([4.667277, -74.091602]).addTo(mymap);
    marker.bindPopup("Salitre Magico").openPopup();

    var circle = L.circle([4.667277, -74.091602], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 5
    }).addTo(mymap);

    //Tercer Milenio
    var marker = L.marker([4.597572, -74.081367]).addTo(mymap);
    marker.bindPopup("Parque Tercer Milenio").openPopup();

    var polygon = L.polygon([
        [4.599684, -74.081902],
        [4.598234, -74.079474],
        [4.595398, -74.081235],
        [4.596818, -74.083878],
        [4.597607, -74.083386],
        [4.597289, -74.082793],
        [4.598038, -74.082349],
        [4.598407, -74.082910],
        [4.598944, -74.082611],
        [4.599173, -74.082496]
    ]).addTo(mymap);

    //Simon Bolivar
    var marker = L.marker([4.658683, -74.093329]).addTo(mymap);
    marker.bindPopup("Parque Simon Bolivar").openPopup();

    var polygon = L.polygon([
        [4.664916, -74.092921],
        [4.664220, -74.094803],
        [4.658958, -74.098869],
        [4.656266, -74.098778],
        [4.652546, -74.093589],
        [4.653211, -74.091920],
        [4.659048, -74.089219],
        [4.660016, -74.089340]
    ]).addTo(mymap);

    

    
}